import './App.css';
import Countries from "./Component/Countries/Countries";
import {useEffect, useState} from "react";
import axios from "axios";
import {COUNTRIES_URL} from "./config";
import Country from "./Component/Country/Country";

const App = () => {
    const [countries, setCountries] = useState([]);
    const [selectedCountry, setSelectedCountry] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get(COUNTRIES_URL);
            const countries = response.data;
            setCountries(countries);
        };
        fetchData().catch(e => console.error(e));
    }, []);

    return (
        <div className="App">
            <div className="Countries">
                {countries.map(country => (
                    <Countries
                        key={country.alpha3Code}
                        name={country.name}
                        onClick={() => setSelectedCountry(country.alpha3Code)}
                    />
                ))};
            </div>
            <div className="Country">
                <Country id={selectedCountry} countries={countries}/>
            </div>
        </div>
    );
};

export default App;

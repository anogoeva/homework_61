import React, {useEffect, useState} from 'react';
import axios from "axios";
import {COUNTRY_URL} from "../../config";

const Country = (props) => {

    const [countryInfo, setCountryInfo] = useState([{defaultMessage: 'Choose a country'}]);

    useEffect(() => {
        const fetchData = async () => {
            if (props.id === null) return;
            const response = await axios.get(COUNTRY_URL + props.id);
            setCountryInfo(response.data);
        };

        fetchData().catch(console.error);
    }, [props]);


    let borders = countryInfo.borders ?
        <p>
            {countryInfo.borders.map((border) =>
                <li key={border}>{border}</li>
            )}
        </p> : null;

    let population = countryInfo.population ?
        <p>Population: <i>{countryInfo.population}</i></p>
        : countryInfo[0].defaultMessage;

    let countryInfoFlag = countryInfo.flag ?
        <img src={countryInfo.flag} alt={countryInfo.name} className="img"/>
        : null;

    return countryInfo && (
        <div>
            <p><strong>{countryInfo.name}</strong></p>
            {borders}
            {population}
            {countryInfoFlag}
        </div>
    );
};

export default Country;
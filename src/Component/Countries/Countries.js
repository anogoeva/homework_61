import React from 'react';

const Countries = (props) => {
    return (
        <div>
            <p onClick={props.onClick}>{props.name}</p>
        </div>
    );
};

export default Countries;